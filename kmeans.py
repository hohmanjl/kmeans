"""
Modified from
Source: https://datasciencelab.wordpress.com/2013/12/12/clustering-with-k-means-in-python/

Usage: $ python kmeans.py 100 3 3

This code is a prototype/work in progress and needs serious refactoring.
Using it to experiment.
"""

import numpy as np
import random
import matplotlib.pyplot as plt
import sys
from matplotlib import cm


def cluster_points(x_, mu):
    clusters = {}
    for x in x_:
        best_mu_key = (
            min([(i[0], np.linalg.norm(x-mu[i[0]]))
                 for i in enumerate(mu)], key=lambda t: t[1])[0]
        )
        try:
            clusters[best_mu_key].append(x)
        except KeyError:
            clusters[best_mu_key] = [x]

    return clusters


def reevaluate_centers(clusters):
    new_mu = []
    keys = sorted(clusters.keys())

    for k in keys:
        new_mu.append(np.mean(clusters[k], axis=0))

    return new_mu


def has_converged(mu, old_mu):
    return set([tuple(a) for a in mu]) == set([tuple(a) for a in old_mu])


def find_centers(x_, k):
    # Initialize to K random centers
    old_mu = random.sample(x_, k)
    mu = random.sample(x_, k)
    clusters = cluster_points(x_, mu)

    while not has_converged(mu, old_mu):
        yield mu, clusters
        old_mu = mu
        # Assign all points in X to clusters
        clusters = cluster_points(x_, mu)
        # Reevaluate centers
        mu = reevaluate_centers(clusters)

    yield mu, clusters


def find_and_plot(x_, k, fig=None):
    i = -1
    colors = cm.rainbow(np.linspace(0, 1, k))

    for mu, clusters in find_centers(x_, k):
        if fig is None:
            continue
        else:
            i += 1
            nki = (len(x_), k, i)
            plot_solution(mu, clusters, fig, colors=colors, nki=nki)

    return mu, clusters, i


def init_board(n):
    x = np.array([(random.uniform(-1, 1), random.uniform(-1, 1)) for i in range(n)])
    return x


def init_board_gauss(n_, k):
    n = float(n_) / k
    x_ = []
    for i in range(k):
        c = (random.uniform(-1, 1), random.uniform(-1, 1))
        s = random.uniform(0.05, 0.5)
        x = []
        while len(x) < n:
            a, b = np.array([np.random.normal(c[0], s), np.random.normal(c[1], s)])
            # Continue drawing points from the distribution in the range [-1,1]
            if abs(a) < 1 and abs(b) < 1:
                x.append([a, b])
        x_.extend(x)
    x_ = np.array(x_)[:n_]
    return x_


def plot(array):
    plt.scatter(array[:, 0], array[:, 1], s=1)
    plt.axis((-1, 1, -1, 1))
    plt.show()


def plot_solution(centers, groups, fig_=None, colors=None, nki=None):
    plt.clf()
    plt.axis([-1, 1, -1, 1])
    fig_.suptitle(
        'N = %d, k = %d, iteration = %d' % (
            nki[0], nki[1], nki[2]
        )
    )

    idx = -1
    for p, c in zip(centers, colors):
        idx += 1
        for x, y in groups[idx]:
            plt.scatter(x, y, c=c)

        plt.scatter(p[0], p[1], s=150, c=c, alpha=0.5)

    fig_.canvas.draw()
    plt.pause(0.01)


def main():
    n = int(sys.argv[1])  # number of elements
    k = int(sys.argv[2])  # number of clusters
    g = int(sys.argv[3])  # number of centers (guess)

    fig = plt.gcf()
    fig.show()
    fig.canvas.draw()

    array = init_board_gauss(n, k)
    mu, clusters, c = find_and_plot(array, g, fig)

    sys.stdout.write('Solution found in %d iterations.\n' % c)
    sys.stdout.flush()

    i = -1
    for m in mu:
        i += 1
        sys.stdout.write('Cluster %02d: (%6s, %6s), Pop: %6d.\n' % (
            i + 1,
            '%0.3f'.rjust(5) % m[0],
            '%0.3f'.rjust(5) % m[1],
            len(clusters[i])
        ))

    sys.stdout.flush()

    raw_input("Enter to exit.")


if __name__ == '__main__':
    main()
