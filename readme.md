# kmeans experimentation

## References
Source/Author: [Clustering With K-Means in Python](https://datasciencelab.wordpress.com/2013/12/12/clustering-with-k-means-in-python/)

## About

I extracted the source from the blog above to perform some experimentation.
Thus, the code is pretty dirty and needs refactoring.
But it is functional to visualize the kmeans clustering algorithm.
__All credit to the original authors__, I only added some modifications for
experimentation purposes.

## Usage
```
$ python kmeans.py <points> <clusters> <k / cluster guess>
```
